
package com.tchamdjeu.sample;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mozer
 */

class Processor1{

    private List<Integer> list =new ArrayList();
    private final int LIMIT=5;
    private final int bottom=0;
    private final Object lock=new Object();
    private int value=0;
    
     public void producer() throws InterruptedException{
        synchronized(lock){
         while(true){
            if(list.size()==LIMIT){
                System.out.println("Waiting for removing items from the list...");
                lock.wait();
            }else{
                System.out.println("Adding: "+value);
                list.add(value);
                value++;
                lock.notify();
            }
            Thread.sleep(1000);
         }

        } 
     }
     
     public void consumer() throws InterruptedException{
        Thread.sleep(1000);
         synchronized(lock){
             while(true){
                
                 if(list.size()==bottom){
                 
                     System.out.println("Waiting for adding items to the list...");
                     lock.wait();
                             
                 }else{
                     
                     System.out.println("Removed: "+list.remove(--value));
                     lock.notify();
                 }
             Thread.sleep(1000);
             }
        } 
     }     

}

public class WaitAndNotify2 {
    
    
    
    public static void main(String[] args) {
        
        Processor1 processor1=new Processor1();
        
        
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor1.consumer();
                } catch (InterruptedException ex) {
                     ex.printStackTrace();
                }
            }
        });
        
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor1.producer();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });        
        
        t2.start();
        t1.start();
        
        try {
            t2.join();
            t1.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
    }    
    
}
