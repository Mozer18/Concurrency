
package com.tchamdjeu.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author mozer
 */
class ProcessorT implements Callable<String>{

    private int id;
    public ProcessorT(int id){
        this.id =id;
    }

    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return "id : "+id;
    }

}


public class Callable_Future {
    
    public static void main(String[] args) {
        
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Future<String>> list=new ArrayList<>();
        
        for (int i = 0; i < 5; i++) {
            Future<String> future= executorService.submit(new ProcessorT(i+1));
            list.add(future);
        }
        
        for (Future<String> future : list) {
            try {
                //invoke the "call" method
                System.out.println(future.get());
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            } catch (ExecutionException ex) {
                ex.printStackTrace();
            }
        }
       //to end the thread 
       executorService.shutdown();
    }
}
