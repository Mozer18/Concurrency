
package com.tchamdjeu.sample;

/**
 *
 * @author mozer
 */
public class SynchronizeBlock {
    
    public static int count1=0;
    public static int count2=0;
    
    //These two objects allow our threads to run the two methods
    //each method is run by one thread at an instant
    //thread A is running method 1 while thread B is running method2
    //not the same method at the same time
    public static Object lock1=new Object();
    public static Object lock2=new Object();
    
    public static void add(){
        //this allow us to tell cpu that thread one don't need to for thread two to use this method
        //only in the case that thread two is not using this method but another one        
        synchronized(lock1){
        count1++;
        }
    }
    
    public static void addAgain(){
        //this allow us to tell cpu that thread one don't need to for thread two to use this method
        //only in the case that thread two is not using this method but another one
        synchronized(lock2){
        count2++;
        }
    }
    
    public static void compute(){
        for (int i = 0; i < 100; i++) {
            add();
            addAgain();
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        double start=System.currentTimeMillis();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                compute();
            }
        });
        
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                compute();
                
            }
        });    
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
        
        System.out.println("count1 ="+count1+" -count2 ="+count2);
        double fin=System.currentTimeMillis();
        System.out.println(fin-start);
    }
    
}
