
package com.tchamdjeu.sample;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author mozer
 */

class Worker{
    
     private Lock lock = new ReentrantLock();
     private Condition condition = lock.newCondition();
    
     public void producer() throws InterruptedException{
         lock.lock();
         System.out.println("We are in the producer method.....");
         condition.await();
         System.out.println("again producer method");
         System.out.println("petit test1...");
         lock.unlock();
         
         
     }
     public void consumer() throws InterruptedException{
        
         lock.lock();
         Thread.sleep(1000);         
         System.out.println("consumer method.....");
         //has the same function as "notify"
         condition.signal();
         //this one allow us to return to the waiting thread
         lock.unlock();
        // Thread.sleep(1000);
         //after the other thread has finished its operations we can return to this instrution
         System.out.println("petit test2...");
        
     }
}

public class WorkerWithLocks {
    
    
    public static void main(String[] args) throws InterruptedException {
        Worker worker =new Worker();
        Thread t1 =new Thread (new Runnable() {
            @Override
            public void run() {
                try {
                    worker.producer();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });

        Thread t2 =new Thread (new Runnable() {
            @Override
            public void run() {
                try {
                    worker.consumer();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });        

        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
    }
    
}
