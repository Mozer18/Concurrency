
package com.tchamdjeu.sample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mozer
 */

enum Downloader{
   
    INSTANCE;
    
    private Semaphore semaphore=new Semaphore(3, true);
    
    public void downloadData(){
    
        try {
            semaphore.acquire();
            download();
            
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }finally{
            semaphore.release();
        }
        
    
    }

    private void download(){
    
        System.out.println("Data is dwonloaded from web....");
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    
    }

}

public class Semaphore1 {
    
    public static void main(String[] args) {
        
        ExecutorService executorService= Executors.newCachedThreadPool();
        
        for (int i = 0; i < 12; i++) {
            
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    
                    Downloader.INSTANCE.downloadData();
                }
            });
        }
    }
    
}
