
package com.tchamdjeu.sample;

/**
 *
 * @author mozer
 */
public class BoucleFor {
    private static int counter=0;
    private static void test(){
         for (int i = 0; i < 10; ++i) {
            counter++;
            System.out.println(i);
        }
    }
    
    public static void main(String[] args) {
        test();
    }
}
