
package com.tchamdjeu.sample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 *
 * @author mozer
 */
public class Executor1 {
    
    public static void main(String[] args) {
        //lancer deux Threads au moment
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        
        for (int i = 0; i < 5; i++) {
            
            executorService.submit(new Worker1());
            
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
               ex.printStackTrace();
            }
            
        }
        executorService.shutdown();
    }
    
}


class Worker1 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }


}