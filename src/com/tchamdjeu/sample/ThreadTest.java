
package com.tchamdjeu.sample;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mozer
 */

class Runner1 extends Thread{

    @Override
    public void run() {
        for(int i=0; i<300 ;i++)
            System.out.println("Runner 1: "+i);    
        try {
            Thread.sleep(100);
            System.out.println("");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }        
    }
}

class Runner2 extends Thread{

    @Override
    public void run() {
        for(int j=0; j<100 ;j++)
            System.out.println("Runner 2: "+j);  
            
        
        try {
            Thread.sleep(100);
            
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

public class ThreadTest {
    
    public static void main(String[] args) throws InterruptedException {

       // Thread t1 = new Thread(new Runner1());
       // Thread t2 = new Thread(new Runner2());
       Runner1 t1 = new Runner1();
       Runner2 t2 = new Runner2();
        
        t1.start();
        t2.start();
        
        //wait thread 1 and thread2 before publishing the message
        t1.join();
        t2.join();
        
        System.out.println("Threads are already finished....");
        
    }
}
