
package com.tchamdjeu.sample;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author mozer
 */
public class ReentranceLock {
    private static int counter=0;
    //allow us to attribut this method to only thread at an instant
    private static Lock lock= new ReentrantLock();
    
    public static void increment(){
        //start the locking
        lock.lock();
        //use try{}finally{} is the good practise
        //finally mean that it's going to unlock the method no matter what
        try{
        for (int i = 0; i < 1000; i++)
            counter++;
        }finally{
        //stop the locking after the incrementation so that others threads 
        //can hold on this method
            lock.unlock();
        }
       
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        Thread t1 =new Thread(new Runnable() {
            @Override
            public void run() {

                increment();
            }
        });
        
        Thread t2 =new Thread(new Runnable() {
            @Override
            public void run() {

                increment();
            }
        });        
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
        
        System.out.println("counter: "+counter);
    }
    
}
