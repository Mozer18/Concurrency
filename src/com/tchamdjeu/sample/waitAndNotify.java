
package com.tchamdjeu.sample;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mozer
 */

class Processor{

     public void producer() throws InterruptedException{
        synchronized(this){
         System.out.println("We are in the producer method.....");
          wait();
            System.out.println("again producer method");
        } 
     }
     public void consumer() throws InterruptedException{
        Thread.sleep(1000);
         synchronized(this){
         System.out.println("consumer method.....");
          notify();
          Thread.sleep(1000);
        } 
     }     

}


public class waitAndNotify {
    
    public static void main(String[] args) {
        
        Processor processor=new Processor();
        
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.producer();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });
        
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.consumer();
                } catch (InterruptedException ex) {
                     ex.printStackTrace();
                }
            }
        });  
        
        t1.start();
        t2.start();
        
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
    }
    
}
